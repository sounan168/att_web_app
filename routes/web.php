<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
//admin route
Route::get('/',[AdminController::class,'home']);
Route::get('register',[AdminController::class,'register']);
Route::get('/login',[AdminController::class, 'index']);
Route::get('/manage_user',[AdminController::class, 'user_manage']);
Route::get('attendance',[AdminController::class,'atten']);
Route::post('login/login_proc',[AdminController::class,'login_pro']);
Route::get('/logout',[AdminController::class,'logout']);
Route::get('/allstd',[AdminController::class,'allstd']);
Route::get('/allteacher',[AdminController::class,'allteacher']);
Route::get('detail',[AdminController::class,'detail']);
//register process
Route::post('/pro_reg',[AdminController::class,'pro_reg']);
//delete route
Route::get('/delete/{id}',[AdminController::class,'delete']);
//update
Route::get('/update',[AdminController::class,'update_user']);
Route::post('/update_pro',[AdminController::class,'update_pro']);
//backup user
Route::get('/backup',[AdminController::class,'backup_user']);
Route::get('/backup_pro/{id}',[AdminController::class,'backup_pro']);
//get attendance user wiht ID
Route::get('/view_atten/{id}',[AdminController::class,'view_atten']);
//search manage

//user route
Route::get('user/user',[UserController::class,'index']);
Route::get('/user_logout',[AdminController::class,'user_logout']);
Route::post('update_img',[UserController::class, 'update_img']);
Route::get('/att_user',[UserController::class,'att_user']);
Route::get('/managepf',[UserController::class,'manage_pf']);
Route::get('checkin_user',[UserController::class,'checkin']);
Route::get('current',[UserController::class,'current']);
//teacher
Route::get('teacher_admin',[AdminController::class,'teacher_admin']);
Route::get('checkin',[UserController::class,'classlist']);
Route::get('toup',[AdminController::class,'up']);
Route::get('checkid',[AdminController::class,'checkid']);


