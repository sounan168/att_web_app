<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login',[ApiController::class,'login']);
Route::get('/getatt',[ApiController::class,'getatt']);
Route::get('/userdetail',[ApiController::class,'detail']);
Route::get('/attmonthly/{filter}/{id}',[ApiController::class,'getattmonthly']);
Route::get('/getbc/{id}',[ApiController::class,'b_c']);
Route::post('checkin/{id}/{class}/{ch}',[ApiController::class,'checkin']);
Route::post('qrscan/{id}/{num}',[ApiController::class,'qrscan']);



