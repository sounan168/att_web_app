<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class mydata extends Model
{
    use HasFactory;
    function getuser($id,$name,$pass){
        return DB::table('usernum')->join('userdetail','userdetail.UserID','usernum.UserID')->where(['userdetail.UserID'=>$id,'UserName'=>$name,'Passwords'=>$pass])->first(['UserName','userdetail.UserID','usernum.Sex','userdetail.Profile']);
    }
    public function getattendance($id, $date){
        return DB::select("select day(selected_date) as x,MONTHNAME(selected_date) as m,n.UserID,n.UserName,time(checkin) as t1,time(checkout) as t2,checkin,checkout, case WHEN checkin is not null or checkout is not null then 'COME' ELSE 'A' end  as ischeck, CASE

        WHEN weekday(selected_date)='0' THEN 'Mon'
        
        WHEN weekday(selected_date)='1' THEN 'Tue'
        
        WHEN weekday(selected_date)='2' THEN 'Wed'
        
        WHEN weekday(selected_date)='3' THEN 'Thu'
        
        WHEN weekday(selected_date)='4' THEN 'Fri'
        
        WHEN weekday(selected_date)='5' THEN 'Sat'
        
        WHEN weekday(selected_date)='6' THEN 'Sun'
        
        END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
         from 
        (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
         from
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
         CROSS JOIN usernum n 
          LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
        where selected_date BETWEEN (CURDATE() - INTERVAL $date) AND CURDATE() and n.UserID= $id
         ORDER BY selected_date");
    }
    public function getdetail($id){
        return DB::table('userdetail')->join('major','major.MajorID','userdetail.MajorID')->join('roles','roles.RolesID','userdetail.RolesID')->join('tbl_group','tbl_group.GenGp','userdetail.GenerationID')
        ->where('UserID',$id)->first();
} 
public function getmonth($filter,$id){
    $date="";
    $count_day="";
    switch ($filter){
        case 1: $date="'2023-01-01' and last_day('2023-01-01')";
                $count_day = "2023-01-01";
        break;
        case 2: $date="'2023-02-01' and last_day('2023-02-01')";
                $count_day = "2023-02-01";
        break;
        case 3: $date="'2023-03-01' and last_day('2023-03-01')";
        $count_day = "2023-03-01";
        break;
        case 4: $date="'2023-04-01' and last_day('2023-04-01')";
        $count_day = "2023-04-01";
        break;
        case 5: $date="'2023-05-01' and last_day('2023-05-01')";
        $count_day = "2023-05-01";
        break;
        case 6: $date="'2023-06-01' and last_day('2023-06-01')";
        $count_day = "2023-06-01";
        break;
        case 7: $date="'2023-07-01' and last_day('2023-07-01')";
        $count_day = "2023-07-01";
        break;
        case 8: $date="'2023-08-01' and last_day('2023-08-01')";
        $count_day = "2023-08-01";
        break;
        case 9: $date="'2023-09-01' and last_day('2023-09-01')";
        $count_day = "2023-09-01";
        break;
        case 10: $date="'2023-10-01' and last_day('2023-10-01')";
        $count_day = "2023-10-01";
        break;
        case 11: $date="'2023-11-01' and last_day('2023-11-01')";
        $count_day = "2023-11-01";
        break;
        case 12: $date="'2023-12-01' and last_day('2023-12-01')";
        $count_day = "2023-12-01";
        break;
    }
    $att=DB::select("select day(selected_date) as x,MONTHNAME(selected_date) as m,time(checkin) as t1,time(checkout) as t2, case WHEN checkin is not null or checkout is not null then 'COME' ELSE 'A' end  as ischeck,Day(LAST_DAY('$count_day')) as day_count,DATE_FORMAT(selected_date,'%a') as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
     from 
    (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
     from
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
     (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
     CROSS JOIN usernum n 
      LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
    where selected_date between $date and n.UserID= $id
    GROUP BY x 
     ORDER BY selected_date");
     $count = DB::select("select  SUM(CASE WHEN checkin IS not NULL or checkout is not null and weekday(selected_date) <> '6' and weekday(selected_date) <> '5'  THEN 1 ELSE 0 END) AS totalcheck,
     SUM(CASE WHEN weekday(selected_date)<>'6' and  weekday(selected_date)<>'5' and (checkin is NULL or checkout is NULL) and date(selected_date)<= date(now()) then 1 ELSE 0 END) AS uncheck
      from 
     (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
      from
      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
      CROSS JOIN usernum n 
       LEFT  JOIN attendance a on a.UserID=n.UserID and ( v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date ) )
     where selected_date between $date and n.UserID=$id
      ORDER BY selected_date");
    return ['count'=>$count,'att'=>$att];
}
public function getBuildingClass($bid){
    $building = DB::table('tbl_building')->get();
    $class = DB::table('class')->where('BID',$bid)->get();
   
    return ['building'=>$building,'class'=>$class];
    
}

public function getcheck($check,$id,$class){
    $validates = DB::select("SELECT * FROM attendance WHERE cast(checkin as date) = date(now()) and UserID = $id");
    $classcheck = DB::table('userdetail')->where('UserID',$id)->first(['ClassID']);
    $now = DB::select("select now()");
    $date = $now[0]->{'now()'};
    
        if($check == '1'){
            if ($class == null){
                return (['message'=>'please select building and class','alert'=>false]);

            }
            if($classcheck->{'ClassID'}!=$class){
                return (['message'=>'Your Class not correct','alert'=>false]);

            }
            if($validates!=null){
                return (['message'=>'You already checkin for today','alert'=>false]);
                // dd($validates);
            }
            DB::table('attendance')->insert(['UserID'=>$id,'checkin'=>$date,'ClassID'=>$class]);
            return(['message'=>'checket','alert'=>true]);
           
        }

        else{

            if ($class == null){
                return (['message'=>'please select building and class','alert'=>false]);

            }
            if($classcheck->{'ClassID'}!=$class){
                return (['message'=>'Your Class not correct','alert'=>false]);

            }

            if($validates!=null){
               
                    DB::select("UPDATE attendance SET checkout= now() WHERE UserID = $id and cast(checkin as DATE) = DATE(now())"); 
                    return (['message'=>'checkout confirmed','alert'=>true]);
                
                
            }
            return (['message'=>'You are not checkin','alert'=>false]);
         }

}
public function scancheck($id,$check){
    $validates = DB::select("SELECT * FROM attendance WHERE cast(checkin as date) = date(now()) and UserID = $id");
    $classcheck = DB::table('userdetail')->where('UserID',$id)->first(['ClassID']);
    $now = DB::select("select now()");
    $date = $now[0]->{'now()'};
    if($check == '1')
    {
        if($validates!=null)
        {
            return (['message'=>'You already checkin','alert'=>false]);
        }
            DB::table('attendance')->insert(['UserID'=>$id,'checkin'=>$date,'ClassID'=>$classcheck->{'ClassID'}]);
            return(['message'=>'checket','alert'=>true]);
    }
    else
    {
        if($validates!=null)
        {
            DB::select("UPDATE attendance SET checkout= now() WHERE UserID = $id and cast(checkin as DATE) = DATE(now())"); 
            return (['message'=>'checkout confirmed','alert'=>true]);
        }
            return (['message'=>'You are not checkin','alert'=>false]);   
    }

}
}
