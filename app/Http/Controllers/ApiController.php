<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\mydata;

class ApiController extends Controller
{
   
  
    function login(Request $r){
        $getuser = new mydata();
        $check = $getuser->getuser($r->id,$r->name,$r->pass);
     
        if($check){
            return response(['data'=>$check,'alert'=>true]);
        }else{
            return response(['data'=>null,'alert'=>false]);
        }
        // return response([$r->all()]);
    }
    function detail(Request $r){
        $dt = new mydata;
        return response([$dt->getdetail($r->id)]);
    }
    function getatt(Request $r){
        $att = new mydata;
        return response(['data'=>$att->getattendance($r->id,$r->date)]);
    }
    function getattmonthly($filter,$id){
        $attmonth = new mydata();
        return response(['data'=>$attmonth->getmonth($filter,$id)]);
    }
    function b_c($bc){
        $buildingclass = new mydata();
        return response($buildingclass->getBuildingClass($bc));
    }
    function checkin($id,$class,$ch){
        $checkin = new mydata();
        return response([$checkin->getcheck($ch,$id,$class)]);
    }
    function qrscan($id,$num){
        $scancheck = new mydata();
        return response()->json($scancheck->scancheck($id,$num));
    }
}
