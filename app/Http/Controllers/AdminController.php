<?php

namespace App\Http\Controllers;

use App\Models\Userdetail;
use Illuminate\Http\Request;
// use DB;
use ArinaSystems\JsonResponse\Facades\JsonResponse;
use App\Models\Usernum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

class AdminController extends Controller
{
    public function test(){
        $user = DB::table('userdetail')->get('UserAvatar');
     }
    function index()
    {
        return view('admin/login');
    }
    function home(){
        if(session('admin')!=null){
            return view('/home');
        }else{
            return redirect('/login');
        }
    }
    function register(){
        if(session('admin')!=null){
            $major = DB::table('major')->get();
            $role =DB::table('roles')->get();
            $gen =DB::table('tbl_generation')->get();
            $class =DB::table('class')->get();
            // foreach($major as $key){
            //     echo $key->{'MajorName'};
            // }
            // echo $major[0]->{'MajorName'};
            return view('register', compact('major','role','gen','class'));
        }else{
            return redirect('/login');
        }
    }
    function pro_reg(Request $r){

      
        $check = DB::table('usernum')->where(['UserID'=>$r->id])->first('UserID');
        
        if($check){
            return redirect('/register')->with('r_error','ID already taken');
        }
        
       if($r->name!=null&&$r->id!=null&&$r->role!=null&&$r->major!=null){
        $genid = $r->gn.'-'.$r->gp;
           $isinsert = DB::table('usernum')->insert
            ([
            'UserID'=>$r->id,
            'UserName'=>$r->name,
            'Sex'=>$r->sex,
            'Passwords'=>$r->pass
            ]);
       
       
        if($isinsert){
            $succ = DB::table('userdetail')->insert(['UserID'=>$r->id,'MajorID'=>$r->major,'RolesID'=>$r->role,'GenerationID'=>$genid,'ClassID'=>$r->class]);
            if($succ){
                return redirect('/register')->with('success','data has been insert successfully');
            }
        } 
       
        
       }else{
          return redirect('/register')->with('r_error','error data insert');
       }
        

    } 
   
       
    
    function atten(){
        
        
        return view('attendance');
    }
    function view_atten(Request $r,$id){
       
        $user = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
        ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
        ->where('usernum.UserID',$id)
        ->where('Active','=',1)
        ->get(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID']);


        if ($r->option!=null){
            $date="";
            $count_day="";
            switch ($r->option){
                case 1: $date="'2023-01-01' and last_day('2023-01-01')";
                        $count_day = "2023-01-01";
                break;
                case 2: $date="'2023-02-01' and last_day('2023-02-01')";
                        $count_day = "2023-02-01";
                break;
                case 3: $date="'2023-03-01' and last_day('2023-03-01')";
                $count_day = "2023-03-01";
                break;
                case 4: $date="'2023-04-01' and last_day('2023-04-01')";
                $count_day = "2023-04-01";
                break;
                case 5: $date="'2023-05-01' and last_day('2023-05-01')";
                $count_day = "2023-05-01";
                break;
                case 6: $date="'2023-06-01' and last_day('2023-06-01')";
                $count_day = "2023-06-01";
                break;
                case 7: $date="'2023-07-01' and last_day('2023-07-01')";
                $count_day = "2023-07-01";
                break;
                case 8: $date="'2023-08-01' and last_day('2023-08-01')";
                $count_day = "2023-08-01";
                break;
                case 9: $date="'2023-09-01' and last_day('2023-09-01')";
                $count_day = "2023-09-01";
                break;
                case 10: $date="'2023-10-01' and last_day('2023-10-01')";
                $count_day = "2023-10-01";
                break;
                case 11: $date="'2023-11-01' and last_day('2023-11-01')";
                $count_day = "2023-11-01";
                break;
                case 12: $date="'2023-12-01' and last_day('2023-12-01')";
                $count_day = "2023-12-01";
                break;
            }
               
            // $start=Carbon::parse($r->start)->format('Y-m-d');
            // $end=Carbon::parse($r->end)->format('Y-m-d');

            $att = DB::select("select day(selected_date) as x,MONTHNAME(selected_date) as m,n.UserID,n.UserName,time(checkin) as t1,time(checkout) as t2,checkin,checkout, case WHEN checkin is not null or checkout is not null then 'COME' ELSE 'A' end  as ischeck, CASE

            WHEN weekday(selected_date)='0' THEN 'Mon'
            
            WHEN weekday(selected_date)='1' THEN 'Tue'
            
            WHEN weekday(selected_date)='2' THEN 'Wed'
            
            WHEN weekday(selected_date)='3' THEN 'Thu'
            
            WHEN weekday(selected_date)='4' THEN 'Fri'
            
            WHEN weekday(selected_date)='5' THEN 'Sat'
            
            WHEN weekday(selected_date)='6' THEN 'Sun'
            
            END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
             from 
            (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
             from
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
             CROSS JOIN usernum n 
              LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
            where selected_date between $date and n.UserID= $id
             ORDER BY selected_date");

             $count = DB::select("select day(selected_date) as x,Day(LAST_DAY('$count_day')) as day_count,MONTHNAME(selected_date) as m,n.UserID,n.UserName,checkin,checkout, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

             WHEN weekday(selected_date)='0' THEN 'Mon'
             
             WHEN weekday(selected_date)='1' THEN 'Tue'
             
             WHEN weekday(selected_date)='2' THEN 'Wed'
             
             WHEN weekday(selected_date)='3' THEN 'Thu'
             
             WHEN weekday(selected_date)='4' THEN 'Fri'
             
             WHEN weekday(selected_date)='5' THEN 'Sat'
             
             WHEN weekday(selected_date)='6' THEN 'Sun'
             
             END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff,SUM(CASE WHEN checkin IS not NULL or checkout is not null and weekday(selected_date) <> '6' and weekday(selected_date) <> '5'  THEN 1 ELSE 0 END) AS totalcheck,
             SUM(CASE WHEN weekday(selected_date)<>'6' and  weekday(selected_date)<>'5' and (checkin is NULL or checkout is NULL) then 1 ELSE 0 END) AS uncheck
              from 
             (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
              from
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
              CROSS JOIN usernum n 
               LEFT  JOIN attendance a on a.UserID=n.UserID and ( v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date ) )
             where selected_date between $date and n.UserID=$id
              ORDER BY selected_date
             ");

             return view('/view_atten',['user'=>$user,'att'=>$att,'count'=>$count]);
             dd($count[0]->{'day_count'});
            //  dd($user[0]->{'UserID'});
        }
            $att = DB::select("select day(selected_date) as x,MONTHNAME(selected_date) as m,time(checkin) as t1,time(checkout) as t2,n.UserID,time(selected_date) as t,n.UserName,checkin,checkout, case WHEN checkin is not null or checkout is not null then 'COME' ELSE 'A' end  as ischeck, CASE

            WHEN weekday(selected_date)='0' THEN 'Mon'
            
            WHEN weekday(selected_date)='1' THEN 'Tue'
            
            WHEN weekday(selected_date)='2' THEN 'Wed'
            
            WHEN weekday(selected_date)='3' THEN 'Thu'
            
            WHEN weekday(selected_date)='4' THEN 'Fri'
            
            WHEN weekday(selected_date)='5' THEN 'Sat'
            
            WHEN weekday(selected_date)='6' THEN 'Sun'
            
            END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
            from 
            (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
            from
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
            CROSS JOIN usernum n 
            LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date ) )
            where selected_date between DATE_FORMAT(CURDATE() ,'%Y-%m-01') AND CURDATE() and n.UserID=$id
            ORDER BY selected_date
        ");
        $count = DB::select("select day(selected_date) as x,Day(LAST_DAY(DATE_FORMAT(CURDATE() ,'%Y-%m-01'))) as day_count,MONTHNAME(selected_date) as m,n.UserID,n.UserName,checkin,checkout, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

        WHEN weekday(selected_date)='0' THEN 'Mon'
        
        WHEN weekday(selected_date)='1' THEN 'Tue'
        
        WHEN weekday(selected_date)='2' THEN 'Wed'
        
        WHEN weekday(selected_date)='3' THEN 'Thu'
        
        WHEN weekday(selected_date)='4' THEN 'Fri'
        
        WHEN weekday(selected_date)='5' THEN 'Sat'
        
        WHEN weekday(selected_date)='6' THEN 'Sun'
        
        END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff,SUM(CASE WHEN checkin IS not NULL or checkout is not null and weekday(selected_date) <> '6' and weekday(selected_date) <> '5'  THEN 1 ELSE 0 END) AS totalcheck,
        SUM(CASE WHEN weekday(selected_date)<>'6' and  weekday(selected_date)<>'5' and (checkin is NULL or checkout is NULL) then 1 ELSE 0 END) AS uncheck
         from 
        (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
         from
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
         CROSS JOIN usernum n 
          LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
        where selected_date between DATE_FORMAT(CURDATE() ,'%Y-%m-01') AND CURDATE() and n.UserID=$id
         ORDER BY selected_date
        ");
        // dd($count[0]->{'day_count'});
         return view('/view_atten',['user'=>$user,'att'=>$att,'count'=>$count]);


        // if($r->start && $r->end != null){
        //     return view('/view_atten',['user'=>$user,'data'=> $data->whereDate("checkin",">=",$r->start)->whereDate("checkin","<=",$r->end)->paginate(7)]);
           
        // }else{
        //     return view('/view_atten',['user'=>$user,'data'=>$data->paginate(7)]);
        // }
        
    }
    function allstd(Request $r){
        $search = $r->search;
        Paginator::useBootstrap();
        $major = DB::table('major')->get();
        $data = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->where('roles.RolesID',2)
        ->where('Active','=',1)
        ->paginate(5);
        if($search){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserID','like','%'.$search.'%')
            ->orWhere('usernum.UserName','like','%'.$search.'%')
            ->where('roles.RolesID',2)
            ->where('Active','=','1')
            ->paginate();       
        }
        if($r->major){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('roles.RolesID',2)
            ->where('Active','=','1')
            ->paginate();
           
        }
        if(is_numeric($search) && $r->roles){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserID','like','%'.$search.'%')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('roles.RolesID',2)
            ->where('Active','=','1')
            ->paginate();
        }
        if(ctype_alpha($search) && $r->roles){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserName','like','%'.$search.'%')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('roles.RolesID',2)
            ->where('Active','=','1')
            ->paginate();
        }
        return view ('student',compact('data','major'));
    }

    function allteacher(Request $r){
        $search = $r->search;
        Paginator::useBootstrap();
        $major = DB::table('major')->get();
        $data = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->where('roles.RolesID',1)
        ->where('Active','=',1)
        ->paginate(5);
        if($search){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserID','like','%'.$search.'%')
            ->orWhere('usernum.UserName','like','%'.$search.'%')
            ->where('roles.RolesID',1)
            ->where('Active','=','1')
            ->paginate();       
        }
        if($r->major){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('roles.RolesID',1)
            ->where('Active','=','1')
            ->paginate();
           
        }
        if(is_numeric($search) && $r->roles){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserID','like','%'.$search.'%')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('roles.RolesID',1)
            ->where('Active','=','1')
            ->paginate();
        }
        if(ctype_alpha($search) && $r->roles){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserName','like','%'.$search.'%')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('roles.RolesID',1)
            ->where('Active','=','1')
            ->paginate();
        }
        return view('teacher',compact('data','major'));
    }
   
    function update_user(Request $r){
        $major = DB::table('major')->get();
            $role =DB::table('roles')->get();
            $gen =DB::table('tbl_generation')->get();
            $gp =DB::table('tbl_group')->get();
           
           
         $data = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
        ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
        ->where('usernum.UserID','=',$r->id)
        ->where('Active','=','1')
        ->orderByRaw('usernum.UserID')
        ->get();
        return view('update',compact('data','major','role','gen','gp'));
        // echo dd($data);
       
    }
    function update_pro(Request $r){
        $data = $r->except('_token','submit','RolesID','MajorID');
        $user = DB::table('usernum')->where('UserID',$data['UserID'])->update($data);
        if($r->RolesID || $r->MajorID || $user){
            DB::table('userdetail')->where('UserID',$data['UserID'])->update(['RolesID'=>$r->RolesID,'MajorID'=>$r->MajorID]);
            return redirect('/manage_user');
        }
    }
    //manage user
    function user_manage(Request $r)
    {
        $search = $r->search;
        $role = DB::table('roles')->get();
        $major = DB::table('major')->get();
        $data = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
        ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
        ->where('Active','=','1')
        ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])
        ->paginate(7);
        Paginator::useBootstrap();
        if($search){
                $data = DB::table('usernum')
                ->join('userdetail','userdetail.UserID','=','usernum.UserID')
                ->join('major','major.MajorID','=','userdetail.MajorID')
                ->join('roles','roles.RolesID','=','userdetail.RolesID')
                ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
                ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
                ->where('usernum.UserID','like','%'.$search.'%')
                ->orWhere('usernum.UserName','like','%'.$search.'%')
                ->where('Active','=','1')
                ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

                ->paginate();       
        }
        if($r->roles){
            $data = DB::table('usernum')
                ->join('userdetail','userdetail.UserID','=','usernum.UserID')
                ->join('major','major.MajorID','=','userdetail.MajorID')
                ->join('roles','roles.RolesID','=','userdetail.RolesID')
                ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
                ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
                ->where('roles.RolesID','like','%'.$r->roles.'%')
                ->where('Active','=','1')
                ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

                ->paginate();
            
        }
        if($r->major){
            $data = DB::table('usernum')
                ->join('userdetail','userdetail.UserID','=','usernum.UserID')
                ->join('major','major.MajorID','=','userdetail.MajorID')
                ->join('roles','roles.RolesID','=','userdetail.RolesID')
                ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
                ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
                ->where('major.MajorID','like','%'.$r->major.'%')
                ->where('Active','=','1')
                ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

                ->paginate();
            
        }
        //role with major
        if($r->roles && $r->major){
            $data = DB::table('usernum')
                ->join('userdetail','userdetail.UserID','=','usernum.UserID')
                ->join('major','major.MajorID','=','userdetail.MajorID')
                ->join('roles','roles.RolesID','=','userdetail.RolesID')
                ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
                ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
                ->where('roles.RolesID','like','%'.$r->roles.'%')
                ->where('major.MajorID','like','%'.$r->major.'%')
                ->where('Active','=','1')
                ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

                ->paginate();
        }
        //id with major
        if(is_numeric($search) && $r->major){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
            ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
            ->where('usernum.UserID','like','%'.$search.'%')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('Active','=','1')
            ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

            ->paginate();
        }
        //name with major
        if(ctype_alpha($search) && $r->major){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
            ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
            ->where('usernum.UserName','like','%'.$search.'%')
            ->where('major.MajorID','like','%'.$r->major.'%')
            ->where('Active','=','1')
            ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

            ->paginate();
        }
        //id with role
        if(is_numeric($search) && $r->roles){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
            ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
            ->where('usernum.UserID','like','%'.$search.'%')
            ->where('roles.RolesID','like','%'.$r->roles.'%')
            ->where('Active','=','1')
            ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

            ->paginate();
        }
        //name with role
        if(ctype_alpha($search) && $r->roles){
            $data = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
            ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
            ->where('usernum.UserName','like','%'.$search.'%')
            ->where('roles.RolesID','like','%'.$r->roles.'%')
            ->where('Active','=','1')
            ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

            ->paginate();
        }
        if(is_numeric($search) && $r->roles && $r->major){
            $data = DB::table('usernum')
                ->join('userdetail','userdetail.UserID','=','usernum.UserID')
                ->join('major','major.MajorID','=','userdetail.MajorID')
                ->join('roles','roles.RolesID','=','userdetail.RolesID')
                ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
                ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
                ->where('usernum.UserID','like','%'.$search.'%')
                ->where('roles.RolesID','like','%'.$r->roles.'%')
                ->where('major.MajorID','like','%'.$r->major.'%')
                ->where('Active','=','1')
                ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

                ->paginate();
            
            
        }
        if(ctype_alpha($search) && $r->roles && $r->major){

            $data = DB::table('usernum')
                ->join('userdetail','userdetail.UserID','=','usernum.UserID')
                ->join('major','major.MajorID','=','userdetail.MajorID')
                ->join('roles','roles.RolesID','=','userdetail.RolesID')
                ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
                ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
                ->where('usernum.UserName','like','%'.$search.'%')
                ->where('roles.RolesID','like','%'.$r->roles.'%')
                ->where('major.MajorID','like','%'.$r->major.'%')
                ->where('Active','=','1')
                ->select(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords'])

                ->paginate();
        }

       
        return view('manage_user',compact('data','role','major'));
       
    }

    
   
    //teacher admin
    function teacher_admin(){
        return view('user/teacher_admin');
    }
   
    function login_pro(Request $r)
    {

        $code = $r->code;
        $email = $r->email;
        $pass = $r->password;
        $role = $r->role;
        $role = 2;

        $user = DB::table('usernum')->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->where('UserName', $email)
            ->where('Passwords', $pass)
            ->where('usernum.UserID', $code)
            ->first();

        $admin = DB::table('admin')
            ->where('name', $email)
            ->where('password', $pass)
            ->first();

        // if ($role == '1') {
        //     if ($user != null && $user->{'RolesID'}==2) {
        //         session(['user' => $user]);
        //         return redirect('user/user');
        //         // echo dd();
        //     }elseif($user != null && $user->{'RolesID'}==1){
        //         session(['teacher'=> $user]);
        //         return redirect('teacher_admin');
        //     }
        //      else {
        //         session()->flash('error','invalid name or passord');
        //         return redirect('/login');
        //     }
        // }
        if ($role == '2') {
            if ($admin != null) {
                session(['admin' => $admin]);
                return redirect('/');
            } else {
                session()->flash('error','invalid name or passord');
                return redirect('/login');
            }
        }
    }
    function backup_user(){
        $data = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
        ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
        ->where('Active','=',0)
        ->get(['usernum.UserName','usernum.UserID','MajorName','Profile','RolesName','Generationname','GroupID','Passwords']);
        return view('backup_user',compact('data'));
    }
  
    function delete($id){
        DB::table('usernum')->where('UserID',$id)->update(['Active'=>'0']);
        return redirect('/manage_user');
    }
    function backup_pro($id){
        DB::table('usernum')->where('UserID',$id)->update(['Active'=>'1']);
        return redirect('/backup');
    }




    function user_logout()
    {
        session()->forget('user');
        
        return redirect('/login');
    }
    function logout()
    {
        session()->forget('admin');
        
        return redirect('/login');
    }
    function detail(Request $r)
    {
        $user_count = [];
        $att = [];
        $gen = DB::table('tbl_generation')->get();
    //  $id = session('user')->{'UserID'};
    if($r->gt!=null && $r->gp!=null && $r->month){
        $gtgp = $r->gt.'-'.$r->gp;
        $date="";
        switch ($r->month){
            case 1: $date="'2023-01-01' and last_day('2023-01-01')";
            $count_day = "2023-01-01";
    break;
    case 2: $date="'2023-02-01' and last_day('2023-02-01')";
            $count_day = "2023-02-01";
    break;
    case 3: $date="'2023-03-01' and last_day('2023-03-01')";
    $count_day = "2023-03-01";
    break;
    case 4: $date="'2023-04-01' and last_day('2023-04-01')";
    $count_day = "2023-04-01";
    break;
    case 5: $date="'2023-05-01' and last_day('2023-05-01')";
    $count_day = "2023-05-01";
    break;
    case 6: $date="'2023-06-01' and last_day('2023-06-01')";
    $count_day = "2023-06-01";
    break;
    case 7: $date="'2023-07-01' and last_day('2023-07-01')";
    $count_day = "2023-07-01";
    break;
    case 8: $date="'2023-08-01' and last_day('2023-08-01')";
    $count_day = "2023-08-01";
    break;
    case 9: $date="'2023-09-01' and last_day('2023-09-01')";
    $count_day = "2023-09-01";
    break;
    case 10: $date="'2023-10-01' and last_day('2023-10-01')";
    $count_day = "2023-10-01";
    break;
    case 11: $date="'2023-11-01' and last_day('2023-11-01')";
    $count_day = "2023-11-01";
    break;
    case 12: $date="'2023-12-01' and last_day('2023-12-01')";
    $count_day = "2023-12-01";
            break;
        }
     $att = DB::select("select selected_date as d,MONTHNAME(selected_date) as m,n.UserID,ifnull(a.UserID,n.UserID)as CID,n.UserName ,s.Day as Day,n.Sex,checkin,checkout,DAYNAME(selected_date) as day, 
     case WHEN checkin is not null or checkout is not NULL then 'Y' ELSE 'A' end  as ischeck, 
               case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff,Day(LAST_DAY('$count_day')) as day_count
               
                  
                      from     
                     (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
                      from
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
                      CROSS JOIN usernum n 
                       LEFT  JOIN attendance a on a.UserID=n.UserID and 

											 (v.selected_date=date(a.checkin) or v.selected_date=date(a.checkout) )
                                             INNER JOIN userdetail dt on dt.UserID = n.UserID
                                             inner join major mj on mj.MajorID = dt.MajorID
                                             INNER join class c on c.calssID=dt.ClassID
                                             INNER JOIN tbl_group tgn on tgn.GenGp = dt.GenerationID
                                              inner join shift s on s.TimeID=tgn.TimeShift
                       
                     where selected_date BETWEEN $date and  dt.GenerationID = '$gtgp' and n.Active = 1 order by selected_date
     
     ");
     $user_count = DB::select("select selected_date as d,MONTHNAME(selected_date) as m,n.UserID as XID,mj.MajorName as major,GenerationName,tgn.GenerationID as GN,tgn.GroupID as GP,n.UserName,ClassName,s.Day as Day,n.Sex,checkin,checkout,DAYNAME(selected_date) as day,IFNULL(a.UserID,n.UserID) as CID, 
     case WHEN checkin is not null and checkout is not NULL then 'Y' ELSE 'A' end  as ischeck, 
               case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff,SUM(CASE WHEN checkin IS not NULL or checkout is not null and weekday(selected_date) <> '6' and weekday(selected_date) <> '5'  THEN 1 ELSE 0 END) AS totalcheck,
               SUM(CASE WHEN weekday(selected_date)<>'6' and  weekday(selected_date)<>'5' and (checkin is NULL or checkout is NULL) then 1 ELSE 0 END) AS uncheck,Day(LAST_DAY('$count_day')) as day_count
               
                  
                      from     
                     (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
                      from
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                      (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
                      CROSS JOIN usernum n 
                       LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=date(a.checkin) or v.selected_date=date(a.checkout))
                       INNER JOIN userdetail dt on dt.UserID = n.UserID
                       inner join major mj on mj.MajorID = dt.MajorID
                       INNER join class c on c.calssID=dt.ClassID
                       INNER JOIN tbl_group tgn on tgn.GenGp = dt.GenerationID
                       inner join tbl_generation tg on tg.GenerationID = dt.GenerationID
                        inner join shift s on s.TimeID=tgn.TimeShift
                       
                     where selected_date BETWEEN $date and dt.GenerationID = '$gtgp' and n.Active = 1  GROUP BY n.UserName 
     
     ");
        // dd($user_count[0]->{'day_count'});
     return view('detail',compact('att','user_count','gen'));
     }

   
        // dd($att);
     return view('detail',compact('att','user_count','gen'));
    }
    function checkid(Request $r){
        $check = DB::table('usernum')->where(['UserID'=>$r->id])->first('UserID');
      echo $check->{'UserID'};

    }
    function up(Request $r){
        $gp=DB::table('tbl_group')->where('GenerationID',$r->test)->get();
        $output ="";
        foreach($gp as $key){
            $output .= '<option value="'.$key->{'GroupID'}.'">'.$key->{'GroupID'}.'</option>';
        }
        echo $output;
    }
}
