<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\Userdetail;
use App\Models\Usernum;
use Carbon\Carbon;
use CloudinaryLabs\CloudinaryLaravel\Facades\Cloudinary;
use Illuminate\Support\Facades\Input;
use Symfony\Component\Console\Input\Input as InputInput;
use Illuminate\Support\Collection;
use PhpParser\Node\Stmt\Switch_;
use JildertMiedema\LaravelPlupload\Facades\Plupload;

class UserController extends Controller
{
    

    public static function getdata($name){
            $detail = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserId',$name)
            ->where('Active','=',1)->get();
            return $detail;
    }
    function index ()
    {
        if(session('user')!=null){
            header('Accept: application/json');
            $name = session('user');
            $data = DB::select("SELECT * from attendance WHERE checkin >CURRENT_DATE-INTERVAL 1 week and UserID = ".$name->{'UserID'}.";");
            $week = DB::select("SELECT * from attendance WHERE checkin >CURRENT_DATE-INTERVAL 2 week and UserID = ".$name->{'UserID'}.";");
            $detail = DB::table('usernum')
            ->join('userdetail','userdetail.UserID','=','usernum.UserID')
            ->join('major','major.MajorID','=','userdetail.MajorID')
            ->join('roles','roles.RolesID','=','userdetail.RolesID')
            ->where('usernum.UserId',$name->{'UserID'})
            ->where('Active','=',1)
            ->get();
            
            // // $name = [1,2,3,4];
            return view('user/user',compact('name','data','detail','week'));
            // echo $name->{'UserName'};
        }else{
            return redirect('/login');
        }
        
        
    }
    //current month user att
    function current(Request $r){
        $name = session('user');
        if ($r->option!=null){
            $date="";
            $count_day="";
            switch ($r->option){
                case 1: $date="'2023-01-01' and last_day('2023-01-01')";
                        $count_day = "2023-01-01";
                break;
                case 2: $date="'2023-02-01' and last_day('2023-02-01')";
                        $count_day = "2023-02-01";
                break;
                case 3: $date="'2023-03-01' and last_day('2023-03-01')";
                $count_day = "2023-03-01";
                break;
                case 4: $date="'2023-04-01' and last_day('2023-04-01')";
                $count_day = "2023-04-01";
                break;
                case 5: $date="'2023-05-01' and last_day('2023-05-01')";
                $count_day = "2023-05-01";
                break;
                case 6: $date="'2023-06-01' and last_day('2023-06-01')";
                $count_day = "2023-06-01";
                break;
                case 7: $date="'2023-07-01' and last_day('2023-07-01')";
                $count_day = "2023-07-01";
                break;
                case 8: $date="'2023-08-01' and last_day('2023-08-01')";
                $count_day = "2023-08-01";
                break;
                case 9: $date="'2023-09-01' and last_day('2023-09-01')";
                $count_day = "2023-09-01";
                break;
                case 10: $date="'2023-10-01' and last_day('2023-10-01')";
                $count_day = "2023-10-01";
                break;
                case 11: $date="'2023-11-01' and last_day('2023-11-01')";
                $count_day = "2023-11-01";
                break;
                case 12: $date="'2023-12-01' and last_day('2023-12-01')";
                $count_day = "2023-12-01";
                break;
            }
               
            // $start=Carbon::parse($r->start)->format('Y-m-d');
            // $end=Carbon::parse($r->end)->format('Y-m-d');

            $att = DB::select("select day(selected_date) as x,MONTHNAME(selected_date) as m,n.UserID,n.UserName,time(checkin) as t1,time(checkout) as t2,checkin,checkout, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

            WHEN weekday(selected_date)='0' THEN 'Mon'
            
            WHEN weekday(selected_date)='1' THEN 'Tue'
            
            WHEN weekday(selected_date)='2' THEN 'Wed'
            
            WHEN weekday(selected_date)='3' THEN 'Thu'
            
            WHEN weekday(selected_date)='4' THEN 'Fri'
            
            WHEN weekday(selected_date)='5' THEN 'Sat'
            
            WHEN weekday(selected_date)='6' THEN 'Sun'
            
            END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
             from 
            (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
             from
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
             CROSS JOIN usernum n 
              LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
            where selected_date between $date and n.UserID= ".$name->{'UserID'}."
             ORDER BY selected_date");

             $count = DB::select("select day(selected_date) as x,Day(LAST_DAY('$count_day')) as day_count,MONTHNAME(selected_date) as m,n.UserID,n.UserName,checkin,checkout, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

             WHEN weekday(selected_date)='0' THEN 'Mon'
             
             WHEN weekday(selected_date)='1' THEN 'Tue'
             
             WHEN weekday(selected_date)='2' THEN 'Wed'
             
             WHEN weekday(selected_date)='3' THEN 'Thu'
             
             WHEN weekday(selected_date)='4' THEN 'Fri'
             
             WHEN weekday(selected_date)='5' THEN 'Sat'
             
             WHEN weekday(selected_date)='6' THEN 'Sun'
             
             END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff,SUM(CASE WHEN checkin IS not NULL and checkout is not null and weekday(selected_date) <> '6' and weekday(selected_date) <> '5'  THEN 1 ELSE 0 END) AS totalcheck,
             SUM(CASE WHEN weekday(selected_date)<>'6' and  weekday(selected_date)<>'5' and (checkin is NULL or checkout is NULL) then 1 ELSE 0 END) AS uncheck
              from 
             (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
              from
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
              (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
              CROSS JOIN usernum n 
               LEFT  JOIN attendance a on a.UserID=n.UserID and ( v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date ) )
             where selected_date between $date and n.UserID=".$name->{'UserID'}."
              ORDER BY selected_date
             ");

             return view('user/currentmonth',['name'=>$name,'att'=>$att,'count'=>$count]);
            // dd($count[0]->{'day_count'});
        }
            $att = DB::select("select day(selected_date) as x,MONTHNAME(selected_date) as m,time(checkin) as t1,time(checkout) as t2,n.UserID,time(selected_date) as t,n.UserName,checkin,checkout, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

            WHEN weekday(selected_date)='0' THEN 'Mon'
            
            WHEN weekday(selected_date)='1' THEN 'Tue'
            
            WHEN weekday(selected_date)='2' THEN 'Wed'
            
            WHEN weekday(selected_date)='3' THEN 'Thu'
            
            WHEN weekday(selected_date)='4' THEN 'Fri'
            
            WHEN weekday(selected_date)='5' THEN 'Sat'
            
            WHEN weekday(selected_date)='6' THEN 'Sun'
            
            END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
            from 
            (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
            from
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
            (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
            CROSS JOIN usernum n 
            LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date ) )
            where selected_date between DATE_FORMAT(CURDATE() ,'%Y-%m-01') AND CURDATE() and n.UserID=".$name->{'UserID'}."
            ORDER BY selected_date
        ");
        $count = DB::select("select day(selected_date) as x,Day(LAST_DAY(DATE_FORMAT(CURDATE() ,'%Y-%m-01'))) as day_count,MONTHNAME(selected_date) as m,n.UserID,n.UserName,checkin,checkout, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

        WHEN weekday(selected_date)='0' THEN 'Mon'
        
        WHEN weekday(selected_date)='1' THEN 'Tue'
        
        WHEN weekday(selected_date)='2' THEN 'Wed'
        
        WHEN weekday(selected_date)='3' THEN 'Thu'
        
        WHEN weekday(selected_date)='4' THEN 'Fri'
        
        WHEN weekday(selected_date)='5' THEN 'Sat'
        
        WHEN weekday(selected_date)='6' THEN 'Sun'
        
        END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff,SUM(CASE WHEN checkin IS not NULL and checkout is not null and weekday(selected_date) <> '6' and weekday(selected_date) <> '5'  THEN 1 ELSE 0 END) AS totalcheck,
        SUM(CASE WHEN weekday(selected_date)<>'6' and  weekday(selected_date)<>'5' and (checkin is NULL or checkout is NULL) then 1 ELSE 0 END) AS uncheck
         from 
        (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
         from
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
         (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
         CROSS JOIN usernum n 
          LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
        where selected_date between DATE_FORMAT(CURDATE() ,'%Y-%m-01') AND CURDATE() and n.UserID=".$name->{'UserID'}."
         ORDER BY selected_date
        ");
        // dd($count[0]->{'day_count'});
         return view('user/currentmonth',['name'=>$name,'att'=>$att,'count'=>$count]);
        // dd(['att'=>$att]);

    }
    function att_user(Request $r){
        if(session('user')!=null){
            Paginator::useBootstrap();
            $id = session('user')->{'UserID'};
            // $att = DB::table('attendance')->where('UserID',$id->{'UserID'});
            $att = DB::select("select selected_date as x,MONTHNAME(selected_date) as m,n.UserID,n.UserName,time(checkin) as t_in,time(checkout) as t_out, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

            WHEN weekday(selected_date)='0' THEN 'Mon'
            
            WHEN weekday(selected_date)='1' THEN 'Tue'
            
            WHEN weekday(selected_date)='2' THEN 'Wed'
            
            WHEN weekday(selected_date)='3' THEN 'Thu'
            
            WHEN weekday(selected_date)='4' THEN 'Fri'
            
            WHEN weekday(selected_date)='5' THEN 'Sat'
            
            WHEN weekday(selected_date)='6' THEN 'Sun'
            
            END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
             from 
            (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
             from
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
             (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
             CROSS JOIN usernum n 
              LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date )) 
            where selected_date between DATE_FORMAT(CURDATE() ,'%Y-%m-01') AND CURDATE() and n.UserID= $id
             ORDER BY selected_date");
            $data = $this->getdata($id);
           // dd($att[1]->{'checkin'});
            if($r->option!=null){
                $date="";
                switch ($r->option){
                    case 1: $date="'2023-01-01' and last_day('2023-01-01')";
                    break;
                    case 2: $date="'2023-02-01' and last_day('2023-02-01')";
                    break;
                    case 3: $date="'2023-03-01' and last_day('2023-03-01')";
                    break;
                    case 4: $date="'2023-04-01' and last_day('2023-04-01')";
                    break;
                    case 5: $date="'2023-05-01' and last_day('2023-05-01')";
                    break;
                    case 6: $date="'2023-06-01' and last_day('2023-06-01')";
                    break;
                    case 7: $date="'2023-07-01' and last_day('2023-07-01')";
                    break;
                    case 8: $date="'2023-08-01' and last_day('2023-08-01')";
                    break;
                    case 9: $date="'2023-09-01' and last_day('2023-09-01')";
                    break;
                    case 10: $date="'2023-10-01' and last_day('2023-10-01')";
                    break;
                    case 11: $date="'2023-11-01' and last_day('2023-11-01')";
                    break;
                    case 12: $date="'2023-12-01' and last_day('2023-12-01')";
                    break;
                }
                   
                $start=Carbon::parse($r->start)->format('Y-m-d');
                $end=Carbon::parse($r->end)->format('Y-m-d');

                $att = DB::select("select selected_date as x,MONTHNAME(selected_date) as m,n.UserID,n.UserName,time(checkin) as t_in,time(checkout) as t_out, case WHEN checkin is null then 'A' when checkout is null then 'A' ELSE 'COME' end  as ischeck, CASE

                WHEN weekday(selected_date)='0' THEN 'Mon'
                
                WHEN weekday(selected_date)='1' THEN 'Tue'
                
                WHEN weekday(selected_date)='2' THEN 'Wed'
                
                WHEN weekday(selected_date)='3' THEN 'Thu'
                
                WHEN weekday(selected_date)='4' THEN 'Fri'
                
                WHEN weekday(selected_date)='5' THEN 'Sat'
                
                WHEN weekday(selected_date)='6' THEN 'Sun'
                
                END as day_name, case when weekday(selected_date)='6' THEN 'off' when weekday(selected_date)='5' then 'off' end as iSoff
                 from 
                (select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
                 from
                 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
                 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
                 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
                 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
                 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v 
                 CROSS JOIN usernum n 
                  LEFT  JOIN attendance a on a.UserID=n.UserID and (v.selected_date=cast(a.checkin as date ) or v.selected_date=cast(a.checkout as date ) )
                where selected_date between $date and n.UserID= $id
                 ORDER BY selected_date");
                 Collection::macro('paginate', function ($perPage, $total = null, $page = null, $pageName = 'page') {
                    $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
                
                    return new LengthAwarePaginator($this->forPage($page, $perPage), $total ?: $this->count(), $perPage, $page, [
                        'path' => LengthAwarePaginator::resolveCurrentPath(),
                        'pageName' => $pageName,
                    ]);
                });
                $rr=collect($att)->paginate(7);
                return view('user/user_attendance',['data'=>$data,'att'=> $rr]);
                
            }
                Collection::macro('paginate', function ($perPage, $total = null, $page = null, $pageName = 'page') {
                    $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
                
                    return new LengthAwarePaginator($this->forPage($page, $perPage), $total ?: $this->count(), $perPage, $page, [
                        'path' => LengthAwarePaginator::resolveCurrentPath(),
                        'pageName' => $pageName,
                    ]);
                });
                $rr=collect($att)->paginate(7);
                return view('user/user_attendance',['data'=>$data,'att'=>$rr]);
                
          
        }else{
            return redirect('login');
        }
       
    }
    
    function manage_pf(){
        if(session('user')!=null){
        $id = session('user');
        $user = DB::table('usernum')
        ->join('userdetail','userdetail.UserID','=','usernum.UserID')
        ->join('major','major.MajorID','=','userdetail.MajorID')
        ->join('roles','roles.RolesID','=','userdetail.RolesID')
        ->join('tbl_group','tbl_group.GenerationID','userdetail.GenerationID')
        ->join('tbl_generation','tbl_generation.GenerationID','tbl_group.GenerationID')
        ->where('usernum.UserID',$id->{'UserID'})
        ->where('Active','=',1)
        ->get();
        return view('user/manage_pf',['user'=>$user]);
        }else{
            return redirect('login');
        }
    }
    function checkin(Request $r){
        
             $building = DB::table('tbl_building')->get();
            $name = session('user');
            $id = $name->{'UserID'};
            $validates = DB::select("SELECT * FROM attendance WHERE cast(checkin as date) = date(now()) and UserID = $id");
            $classcheck = DB::table('userdetail')->where('UserID',$id)->first(['ClassID']);
            $now = DB::select("select now()");
            $date = $now[0]->{'now()'};
            
                if($r->check == '1'){
                    if ($r->class == null){
                        return redirect('checkin_user')->with('error','please select building and class');

                    }
                    if($classcheck->{'ClassID'}!=$r->class){
                        return redirect('checkin_user')->with('error','Your Class not correct');

                    }
                    if($validates!=null){
                        return redirect('checkin_user')->with('error','You already checkin for today');
                        // dd($validates);
                    }
                  
                    // dd('no helo');
                   DB::table('attendance')->insert(['UserID'=>$name->{'UserID'},'checkin'=>$date,'ClassID'=>$r->class]); 
                   
                    return redirect('checkin_user')->with('confirm','Checkin is comfirmed');
                   
                }
                if($r->check == '2'){
                    if($validates!=null){
                       
                            DB::select("UPDATE attendance SET checkout= now() WHERE UserID = $id and cast(checkin as DATE) = DATE(now())"); 
                            return redirect('checkin_user')->with('success','checkout confirmed');
                        
                        
                    }
                    return redirect('checkin_user')->with('error','You are not checkin');
                 }
            
            
           
           
            return view('user/checkin',['name'=>$name,'building'=>$building]);
            
       }
       function classlist(Request $r){
        $class = DB::table('class')->where('BID',$r->classid)->get();
        $output ="";
        foreach($class as $key){
            $output .= '<option value="'.$key->{'calssID'}.'">'.$key->{'ClassName'}.'</option>';
        }
        echo $output;
    }
       
    function update_img(Request $r)
    {
        // if(session('user')!=null && $r->avatar!=null){
        //     $image_file = $r->avatar;
            
        //     $image = Image::make($image_file);
            
        //    Response::make($image->encode('jpeg'));
            $id = session('user');
        //    $isupdate= DB::table('userdetail')->where('UserID',$id->{'UserID'})->update(['UserAvatar'=>$image]);
        //     if($isupdate){
        //        return redirect('managepf');
        //     }
        //    }else{
        //     return redirect('login');
        //    }
        //    }
    
    //    if(session('user')!=null){
        
        $file= $r->imgs;
        if($file!=null){
            
        $imgname = $id->{'UserName'}.'_'.Str::random(32).".".$file->getClientOriginalExtension();
        $profile = DB::table('userdetail')->where('UserID',$id->{'UserID'})->get('Profile');
       
            if ($profile[0]->{'Profile'}!=null){
                Storage::disk('public')->delete($profile[0]->{'Profile'});
    
                Storage::disk('public')->put($imgname,file_get_contents($r->imgs));
                $isupdate= DB::table('userdetail')->where('UserID',$id->{'UserID'})->update(['Profile'=>$imgname]);
               
                if($isupdate){
                        return redirect()->back();
                }
               
            }else{
                Storage::disk('public')->put($imgname,file_get_contents($r->imgs));
                $isupdate= DB::table('userdetail')->where('UserID',$id->{'UserID'})->update(['Profile'=>$imgname]);
               
                if($isupdate){
                        return redirect()->back();
                }
            }
        }else{
            return redirect()->back()->with('error','Can not save emty file');
        }
        
       
      
        // dd($profile[0]->{'Profile'});
        
        
        // if(Storage::disk('public')->exists('image/VTFlLKzfskO881JsApgeieasTZdJdk6E.png')){
        //     Storage::disk('public')->delete('image/VTFlLKzfskO881JsApgeieasTZdJdk6E.png');
        //     echo 'ok';
        // }
        // dd(public_path('image'));
  

      
        // $image_file = $r->avatar;
        // $image = Image::make($image_file);
       
        // dd($image_file->getRealPath());
   

 
      
    //     $image= $r->imgs;
    //    Response::make($image->encode('jpeg'));
    //     $id = session('user');
        // dd($image_file);
        
      
    //    }else{
    //     return redirect('login');
    //    }
    //    }

    }

   
}
