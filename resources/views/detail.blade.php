@extends('layout')
@section('leftnav')
@include('views.style')
@stop
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
<style>

    @media print {
        @page {
            size: A4 landscape;
        }

        #page-print thead tr th {
            border: 1px solid #000000 !important;
            /* background-color: #92CDDC !important; */
        }

        #page-print tfoot tr td {
            border: 1px solid #000000 !important;
        }

        #page-print tbody tr th,
        #page-print tbody tr td {
            border: 1px solid #000000 !important;
        }
    }

    thead tr th,
    thead tr td {
        white-space: nowrap;
        /* background: #92CDDC !important; */
        color: #000000 !important;
        padding: 7px !important;
        text-align: center
    }

    tbody tr td {
        color: #000000 !important;
    }

    tfoot tr td {
        white-space: nowrap;
        /* background: #92CDDC !important; */
        color: #000000 !important;
        padding: 7px !important;
        /* text-align: center; */
    }

    .cell_bg td {
        /* background: #e2efda;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           color: #000; */
        padding: 6px !important;
    }

    .current_job {
        background: #3fb0f1;
        border-color: #96a38e !important;
    }

    tbody tr td {
        white-space: nowrap;
        border-color: #000000 !important;
        padding: 8px !important;
    }

    .table-bordered td,
    .table-bordered th {
        border: 1px solid #000000 !important;
    }

    .report-title {
        color: #101011;
    }

    .interview_date_bg {
        background: #643ad8;
    }

    .yellow_bg {
        background: #fce598;
    }

    tbody tr td {
        border-color: #ddd !important;
    }

    tr .text-roted {
        writing-mode: vertical-lr;
        -webkit-transform: rotate(-180deg);
        -moz-transform: rotate(-180deg);
    }

    .text-p {
        background-color: #fec004 !important;
    }

    .side-right p {

        color: #000000 !important;
    }

    .b-yellow {
        background-color: #ffff04 !important;
        color: #000000 !important;
    }
    .opacity{
        opacity: 0.5;
    }
</style>
<div class="container-fluid">
    <!-- Content Wrapper. Contains page content -->

    <!-- /.content-header -->

    <!-- Main content -->




    <!-- Content Header (Page header) -->
    <div class="card small-card">
        <div class="card-body py-1 pb-2">
            <div class="row">

                <div class="col-md-12">

                    <div class="table-responsive">

                        {{-- filter --}}
                        <form action="{{url('detail')}}" method="get" style="font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif" class="">
                        <div class="form-row">
                            <div class="form-group col-md-3" >

                                <label class="form-control text-left pl-1 pt-0 m-0" style="background: none;border: none; height: auto;" for="gt">Generation</label>
                                <select name="gt" id="gn" class="form-control opacity">
                                    <option value="">-Generation-</option>
                                    @foreach($gen as $g)
                                    <option value="{{$g->GenerationID}}">{{$g->GenerationID}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group col-md-3">

                                <label class="form-control text-left pl-1 pt-0 m-0" style="background: none;border: none; height: auto;" for="gt">Group</label>
                                <select name="gp" id="gp" class="form-control opacity">
                                    <option value="">-Group-</option>
                                </select>

                            </div>
                            <div class="form-group col-md-3">

                                <label class="form-control text-left pl-1 pt-0 m-0" style="background: none;border: none; height: auto;" for="gt">Month</label>
                                <select name="month" id="gp" class="form-control opacity">
                                    <option value="">-Month-</option>
                                    <?php
                                        for($m=1; $m<=12; ++$m)
                                        {
                                            echo '<option value='.$m.'>'.date('F', mktime(0, 0, 0, $m, 1)).'</option>';
                                        }
                                    ?>
                                </select>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="form-control text-left pl-1 pt-0 m-0" style="background: none;border: none; height: auto;" for="gt">Action</label>
                                <button class="btn btn-info form-control" type="submit">
                                <i class="fa fa-search"></i>&nbsp;&nbsp;Filter</button>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="form-control text-left pl-1 pt-0 m-0" style="background: none;border: none; height: auto;" for="gt">Action</label>
                                <button class="btn btn-info form-control" type="submit"  onclick = "printResult()">
                                <i class="fa fa-print">
                                </i>&nbsp;&nbsp;Print</button>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="form-control text-left pl-1 pt-0 m-0" style="background: none;border: none; height: auto;" for="gt">Action</label>
                                <button class="btn btn-info form-control" id="btnExport" type="submit"  onclick="exportReportToExcel(this)">
                                <i class="fa-solid fa-file-excel">
                                </i>&nbsp;&nbsp;Excel</button>
                            </div>
                        </div>
                    </form>
{{-- filter --}}



<div class="card small-card">
    <div class="card-body" id="printDocument">
        <div class="print-content">
            <div class="print-body">
                    <div style="display: flex;">
                        <div id="print" style="padding-top: 12px !important;width:100%; background-color: #fff !important;">
                            <center><h1 style="font-size: 1.5rem; font-weight: bold;">បញ្ជីតាមដានវត្តមាននិស្សិត</h1></center>
                            <div style="display: flex;">
                                <div style="flex-basis: ">
                                    <p>បន្ទប់បង្រៀន <b class="classroom">{{$att==null||$user_count==null?'...................':$user_count[0]->ClassName}}</b></p>
                                    <p>មុខវិជ្ជាបង្រៀន <b class="subject">{{$att==null||$user_count==null?'...................':$user_count[0]->major}}</b></p>
                                </div>
                                <div style="flex-grow: 1; display: flex; flex-direction: column; justify-content: flex-end; align-items: center;">
                                </div>
                                <div style="flex-basis: 200px">
                                    <p>វេនសិក្សា <b id="shift">{{$att==null||$user_count==null?'..........':$user_count[0]->Day}}</b></p>
                                    <p>ជំនាន់<b id="year_ti"></b><b id="promotion">{{$att==null||$user_count==null?'...................':$user_count[0]->GN}}</b>ក្រុម<b id="semester">{{$att==null||$user_count==null?'...................':$user_count[0]->GP}}</b></p>
                                    <p>ឆ្នាំសិក្សា <b id="academic_year">{{$att==null||$user_count==null?'...................':$user_count[0]->GenerationName}}</b></p>
                                </div>
                            </div>
                            <?php
                            use Carbon\Carbon;
                            $now = Carbon::now('PST');
                            $td30 = '';
                            $th30 = '';
                            if($att != null){
                                for ($i = 1; $i <= $user_count[0]->{'day_count'}; $i++) {

                                $th30 .= '<th>' . $i . '</th>';
                            }
                            }else{
                                for ($i = 1; $i <= 30; $i++) {

                                $th30 .= '<th>' . $i . '</th>';
                            }
                            }

                            ?>
                            <table class="table-bordered table-hover student-attendance-table table print-table print-table-bordered" id="page-print">
                                <thead class="print-body">
                                    <tr style="height: 30px;" class="text-center">
                                        <td rowspan="2">UserID</td>
                                        <td rowspan="2">ឈ្មោះខ្មែរ</td>
                                        <td rowspan="2">ភេទ</td>
                                        <td id="date_by_study_time" colspan="{{$att==null?'30':$user_count[0]->day_count}}">វត្តមាន</td>
                                        <td rowspan="2" class="b-yellow"><span class="text-roted">សរុបវត្តមាន</span></td>
                                        <td rowspan="2" class="b-yellow"><span class="text-roted">អវត្តមានសរុប</span></td>
                                        <td rowspan="2" class="b-yellow"><b>ផ្សេងៗ</b></td>
                                    </tr>
                                    {{-- <tr id="daterange1">
                                        {!! $td30 !!}
                                    </tr> --}}
                                    <tr id="daterange2">
                                        {!! $th30 !!}
                                    </tr>
                                </thead>
                                <thead>
                                    @if($att!=null || $user_count!=null)
                                    @foreach($user_count as $key)
                                    <tr>
                                        <td>{{$key->XID}}</td>
                                        <td>{{$key->UserName}}</td>
                                        <td>{{$key->Sex}}</td>
                                        @foreach($att as $a)
                                        @if($key->{'XID'} == $a->{'CID'})

                                        <td class="
                                        <?php
                                        if($a->{'iSoff'}=='off') {
                                            echo 'bg-red';
                                        }
                                        else if($a->{'ischeck'}=='Y'){
                                            echo 'bg-info';

                                        }

                                        else{
                                            echo 'bg-yellow';
                                        }
                                        ?>
                                        "><?php
                                        if($a->{'iSoff'}=='off') {
                                            echo 'OFF';
                                        }
                                        else if($a->{'ischeck'}=='Y'){
                                            echo 'Y';

                                        }

                                        else{
                                            echo 'A';
                                        }
                                        ?></td>
                                        @endif
                                        @endforeach
                                        <td>{{$key->totalcheck}}</td>
                                        <td>{{$key->uncheck}}</td>
                                        <td></td>
                                    </tr>

                                    @endforeach
                                    @endif


                                </thead>

                                <tbody class="">

                                </tbody>
                        </div>
                        </div>
                    </table>
                    <br>
                    <div style="display: flex;">
                        <div style="flex-basis: 200px">
                            <p>សំគាល់៖</p>
                            <p>Y= វត្តមាន (មករៀន)</p>
                            <p>A= អវត្តមាន(គ្មានច្បាប់)</p>
                            <p>P = អវត្តមាន (មានច្បាប់)</p>
                        </div>
                        <div style="flex-grow: 1; display: flex; flex-direction: column; justify-content: flex-end; align-items: center;">
                        </div>
                        <div style="">
                            <p style="text-aling:center">ថ្ងៃ ......... ខែ .......... ឆ្នាំ ............ ព.ស ២៥....</p>
                            <p style="text-aling:center"><span class="campus_name"></span> ថ្ងៃទី {{$now->format('d')}} ខែ {{$now->format('m')}} ឆ្នាំ 20{{$now->format('y')}} </p>
                        </div>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        //  $(document).ready(function () {

        // On button click, get value
        // of input control Show alert
        // message box

        //   $("#gn").change(function(){
        //     $.ajaxSetup({
        //             headers: {
        //                 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        //             }
        //         });
        //         $.ajax({
        //         type: "GET",
        //         url: "path/to/server/script.php",
        //         data: "id="+$("#program").val(),
        //         success: function() {
        //             console.log()
        //         }
        //     //     var g = $("#gn")
        //     //    $("#gp").html(`<option>${g.val()}</option>`)
        //   })
        // });
        $(document).ready(function() {
            $("#gn").change(function(e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                var type = "get";
                var ajaxurl = 'toup';
                var fdata = {
                    test: $("#gn").val()
                }
                $.ajax({
                    type: type,
                    url: ajaxurl,
                    data: fdata,

                    success: function(respone) {
                        //var data = JSON.parse(respone)
                        $("#gp").html(respone)
                        //  data.map((e,i)=>{
                        //     $("#gp option").each(function(i){
                        //          option").html(e.GroupID)
                        //     })
                        //  })

                            // var sec = $("#gp")
                            // $.each(data, function(index, value) {
                            //     $("<option/>", {
                            //         value: index,
                            //         text: value.GroupID
                            //     }).appendTo(sec);
                            // });

                    },

                });
            });
        })
        function printResult() {
            var mywindow = window.open('', 'PRINT');
            mywindow.document.write(document.getElementById('stylePrintContent').outerHTML);
            mywindow.document.write(document.getElementById('printDocument').innerHTML);

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();

            return true;
        }
        function exportReportToExcel() {
        let table = document.getElementsByTagName("table");
        TableToExcel.convert(table[0], {
            name: `export.xlsx`,
            sheet: {
            name: 'Sheet 1'
            }
        });
        }
    </script>
    <!-- /.row -->

    @stop
    {{-- @include('views.script') --}}
