<style id="stylePrintContent">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Hanuman:wght@300&display=swap" rel="stylesheet">
        .print-content {
            padding: 0;
            margin: 0;
            width: 100%;
            font-family: 'Times New Roman', Times, serif;
            font-family: 'Hanuman', serif;
            font-size: 12px;
        }

        .print-content .print-header {
            width: 100%;
            margin-top: 10px;
            margin-bottom: 10px;
        }

        .print-content .print-header .print-header-logo {
            width: 100%;
            margin-bottom: 10px;
        }

        .print-content .print-header .print-header-logo .print-header-logo-between {
            width: 100%;
            display: flex;
            justify-content: space-between;
        }

        .print-content .print-header .print-header-logo .print-header-logo-between img {
            width: 40%;
            object-fit: cover;
            background-repeat: no-repeat;
        }

        .print-content .print-header .print-header-title {
            width: 100%;
            font-weight: bold;
            margin-bottom: 10px;
            font-size: 16px;
        }

        .print-content .print-header .print-header-title-main {
            font-size: 16px;
        }

        .print-content .print-header .print-header-title-sub {
            font-size: 14px;
        }

        .print-content .print-header .print-header-title .print-header-title-center {
            width: 100%;
            margin-bottom: 10px;
            text-align: center;
        }

        .print-content .print-header .print-header-title .print-header-title-left {
            width: 100%;
            margin-bottom: 10px;
            text-align: left;
        }

        .print-content .print-header .print-header-title .print-header-title-right {
            width: 100%;
            margin-bottom: 10px;
            text-align: right;
        }

        .print-content .print-body {
            width: 100%;
            font-family: 'Hanuman', serif;
        }

        .print-content .print-body .print-table {
            width: 100%;
            margin-bottom: 10px;
            font-size: 12px;
        }

        .print-content .print-body .print-table.print-table-bordered {
            border-collapse: collapse;
        }

        .print-content .print-body .print-table.print-table-bordered thead tr th td,
        .print-content .print-body .print-table.print-table-bordered thead tr th th,
        .print-content .print-body .print-table.print-table-bordered tbody tr  th td,
        .print-content .print-body .print-table.print-table-bordered tbody tr  th th,
        .print-content .print-body .print-table.print-table-bordered tfoot tr th td,
        .print-content .print-body .print-table.print-table-bordered tfoot tr  thth,
        .print-content .print-body .print-table.print-table-bordered tr td,
        .print-content .print-body .print-table.print-table-bordered tr th {
            border: 1px solid black;
            padding: 5px 8px;
        }

        .print-w-10{
            width: 10%;
        }

        .print-w-20{
            width: 20%;
        }

        .print-w-30{
            width: 30%;
        }

        .print-w-40{
            width: 40%;
        }

        .print-w-50{
            width: 50%;
        }

        .print-w-60{
            width: 60%;
        }

        .print-w-70{
            width: 70%;
        }

        .print-w-80{
            width: 80%;
        }

        .print-w-90{
            width: 90%;
        }

        .print-w-100{
            width: 100%;
        }

        .print-float-left{
            float: left;
        }

        .print-float-right{
            float: right;
        }

        .print-text-left {
            text-align: left;
        }

        .print-text-right {
            text-align: right;
        }

        .print-text-center {
            text-align: center;
        }

        .print-mt-1 {
            margin-top: 5px;
        }

        .print-mt-2 {
            margin-top: 10px;
        }

        .print-mt-3 {
            margin-top: 15px;
        }

        .print-mt-4 {
            margin-top: 20px;
        }

        .print-mb-1 {
            margin-bottom: 5px;
        }

        .print-mb-2 {
            margin-bottom: 10px;
        }

        .print-mb-3 {
            margin-bottom: 15px;
        }

        .print-mb-4 {
            margin-bottom: 20px;
        }

        .print-ml-1 {
            margin-left: 5px;
        }

        .print-ml-2 {
            margin-left: 10px;
        }

        .print-ml-3 {
            margin-left: 15px;
        }

        .print-ml-4 {
            margin-left: 20px;
        }

        .print-mr-1 {
            margin-right: 5px;
        }

        .print-mr-2 {
            margin-right: 10px;
        }

        .print-mr-3 {
            margin-right: 15px;
        }

        .print-mr-4 {
            margin-right: 20px;
        }

        .print-m-1 {
            margin: 5px;
        }

        .print-m-2 {
            margin: 10px;
        }

        .print-m-3 {
            margin: 15px;
        }

        .print-m-4 {
            margin: 20px;
        }


        .print-pt-1 {
            padding-top: 5px;
        }

        .print-pt-2 {
            padding-top: 10px;
        }

        .print-pt-3 {
            padding-top: 15px;
        }

        .print-pt-4 {
            padding-top: 20px;
        }

        .print-pb-1 {
            padding-bottom: 5px;
        }

        .print-pb-2 {
            padding-bottom: 10px;
        }

        .print-pb-3 {
            padding-bottom: 15px;
        }

        .print-pb-4 {
            padding-bottom: 20px;
        }

        .print-pl-1 {
            padding-left: 5px;
        }

        .print-pl-2 {
            padding-left: 10px;
        }

        .print-pl-3 {
            padding-left: 15px;
        }

        .print-pl-4 {
            padding-left: 20px;
        }

        .print-pr-1 {
            padding-right: 5px;
        }

        .print-pr-2 {
            padding-right: 10px;
        }

        .print-pr-3 {
            padding-right: 15px;
        }

        .print-pr-4 {
            padding-right: 20px;
        }

        .print-p-1 {
            padding: 5px;
        }

        .print-p-2 {
            padding: 10px;
        }

        .print-p-3 {
            padding: 15px;
        }

        .print-p-4 {
            padding: 20px;
        }

        @media print{
            @page{
                margin: 10px;
                padding: 0;
            }
        }

    </style>
