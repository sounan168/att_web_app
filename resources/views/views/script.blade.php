
@push('script')
    <script>
        function printResult() {
            var mywindow = window.open('', 'PRINT');
            mywindow.document.write(document.getElementById('stylePrintContent').outerHTML);
            mywindow.document.write(document.getElementById('printDocument').innerHTML);

            mywindow.document.close(); // necessary for IE >= 10
            mywindow.focus(); // necessary for IE >= 10*/

            mywindow.print();
            mywindow.close();

            return true;
        }
    </script>
@endpush
