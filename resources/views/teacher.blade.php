@extends('layout')
@section('leftnav')
Teacher
@stop
@section('content')
<div class="container">
<table class="table">
  <thead>
  </thead>
  <tbody class="table-group-divider table-divider-color">
   @foreach($data as $key)
    <tr>
      <td class="text-left">
        <div class="d-flex align-items-center">
        @if($key->{'Profile'}==null)
                                        <img src="/dist/img/no_avatar.png" style="width: 60px; height: 60px" class="rounded-circle img-fluid img-thumbnail " />
                                        @elseif($key->{'Profile'}!=null)
                                        <img src="{{asset('image/'.$key->Profile.'')}}" style="width: 60px; height: 60px" class="rounded-circle img-fluid img-thumbnail " />
                                        @endif
       <p class="h5 ml-3  mb-0">
          
         <strong>{{ $key->UserName}}</strong>
         <br>
         <span style="font-size: 20px;">ID:{{$key->UserID}}</span>
         <br>
         <small class="text-muted h5">{{$key->MajorName}}</small>
          
       </p>
       
        </div>
      
      
      </td>
      <td  class="text-right"><a href="{{url('/view_atten/'.$key->UserID.'')}}" class="btn btn-info" >View</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$data->links()}}
</div>
@stop