@extends('layout')


@section('leftnav')
View Attendance
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card " style="width: 18rem; border-radius: 20px;">
                <div class="card-img-top d-flex justify-content-center pt-4">
                @if($user[0]->{'Profile'}==null)
                                        <img src="/dist/img/no_avatar.png" style="width: 100px; height: 100px" class="rounded-circle img-fluid img-thumbnail " />
                                        @elseif($user[0]->{'Profile'}!=null)
                                        <img src="{{asset('image/'.$user[0]->Profile.'')}}" style="width: 100px; height: 100px" class="rounded-circle img-fluid img-thumbnail " />
                                        @endif
                </div>
                <div class="card-body">

                    <table class="table">

                        <tbody>
                            <tr>
                                <th scope="row" class="text-uppercase">name</th>
                                <td class="text-uppercase">{{$user[0]->UserName}}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-uppercase">id</th>
                                <td class="text-uppercase">{{$user[0]->UserID}}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-uppercase">role</th>
                                <td class="text-uppercase">{{$user[0]->RolesName}}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-uppercase">major</th>
                                <td class="text-uppercase">{{$user[0]->MajorName}}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-uppercase">generation</th>
                                <td class="text-uppercase">{{$user[0]->Generationname}}</td>
                            </tr>
                            <tr>
                                <th scope="row" class="text-uppercase">group</th>
                                <td class="text-uppercase">{{$user[0]->GroupID}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <!--Table-->
            <h1>{{$att[0]->m}}</h1>
        <form class="" action="{{url('/view_atten/'.$user[0]->UserID)}}">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <div class="form-row">
                            <div class="form-group col-md-4 sl">
                               <label for="">Select Month</label>
                               <select name="option" id="" class="form-control">
                                <option value="">-Month-</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">Novembe</option>
                                <option value="12">December</option>
                              </select>
                                
                            </div>
                            <div class="form-group col-md-3 b-f">
                                <label for=""></label>
                               
                                   
                                    <button type="submit" class="btn btn-info">Filter</button>

                                
                                    <a href="{{url('/view_atten/'.$user[0]->UserID.'')}}" class="btn btn-info ">Current</a>

                                    
                            
                          
                           
                            </div>
                        </div>
                    </div>
                    
                </div>
            </form>
            <div style="width: 100%; overflow-x: scroll;">
            <table class="table table-borderless tb">
            <thead>
                <tr class="bg-red">
                  
                    <th class="text-center align-middle" style="white-space: nowrap; background-color: #aaf1f3;  color: black;"rowspan="2">Name</th>
                    @for($i=1; $i<=$count[0]->{'day_count'};$i++)
                    
                    <th class="text-center" style="background-color: #aaf1f3; border-bottom: 1px solid black; color: rgb(0, 0, 0);">{{$i}}</th>
                   
                    @endfor

                    
                </tr>
                <tr>    
                    @foreach($att as $key)
                    <th class="text-center {{$key->iSoff=='off'?'bg-red':''}}" style="background-color: #aaf1f3; color: black;  white-space: nowrap;border-bottom: 1px solid black;">{{$key->day_name}}</th>
                    @endforeach
                    
                </tr>
            </thead>
            <tbody>
               <tr>
                    <td class="collapse" id="navbarToggle">Time</td>
                    @foreach($att as $key)
                    <td class="collapse" id="navbarToggle" style="white-space: nowrap;">{{$key->t1==null?'??-??-??':$key->t1}} - {{$key->t2==null?'??-??-??':$key->t2}}</td>
                    @endforeach
                  
                </tr>
                <tr class="" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">
                    <td class=" align-middle" style="white-space: nowrap; background-color: #aaf1f3; color: black;">{{$att[0]->UserName}}</td>
                   
                    @foreach($att as $key)
                    <td class="bg-light text-center alig-middle" style="font-size: 25px; box-shadow:inset 0px 0px 0px 0.1px black;
"><i class="<?php
            if ($key->{'ischeck'} == 'COME' && $key->{'iSoff'} != 'off') {
                echo 'fa-solid fa-circle-check text-blue';
            } elseif ($key->{'iSoff'} == 'off') {

                echo 'fa-solid fa-circle text-red';
            } else {
                echo 'fa-solid fa-circle-xmark text-yellow';
            }
            ?>"></i></td>
                    @endforeach
                </tr>
            </tbody>
        </table>
            </div>
        
        <div  class="row p-2 sumarry" >

            <div class="col-md-12 border p-0">
                <h3 class="bg-light">Summary of {{$att[0]->m}}</h3>
                <table class="table table-borderless ">
                    <tr class="h5">
                        <th class="">Missing</th>
                        <th class=""><i class="fa-solid fa-circle-xmark text-red"></i> : {{$count[0]->uncheck}}</th>
                    </tr>
                    <tr class="h5">
                        <th class="">Attend</th>
                        <th class=""><i class="fa-solid fa-circle-check text-blue"></i> : {{$count[0]->totalcheck}}
                        </th>
                    </tr>
                </table>
            
        </div>
    </div>
</div>
@stop