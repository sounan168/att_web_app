@extends('layout')
@section('leftnav')
Register
@stop
@section('content')
<!-- /.body -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
        <div class="container">
          @if(Session::has('success'))
          <div class="alert  alert-dismissible fade show" style="background-color: rgba(3, 252, 19,.3); border: solid 1px rgba(3, 252, 19);box-shadow: rgba(3, 252, 19, 0.25) 0px 0.0625em 0.0625em, rgba(3, 252, 19, 0.25) 0px 0.125em 0.5em, rgba(3, 252, 19, 0.1) 0px 0px 0px 1px inset;" role="alert">
             <strong>Success</strong> {{session('success')}}.
             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
          </div>
          @endif
          @if(Session::has('r_error'))
          <div class="alert  alert-dismissible fade show" style="background-color: red;" role="alert">
             <strong>Failed</strong> {{session('r_error')}}.
             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
             <span aria-hidden="true">&times;</span>
             </button>
          </div>
          @endif
       
          <h1 class="text-center mb-3 pt-3 pb-3 bg-info rounded">Register</h1>
          <form action="{{url('/pro_reg')}}" method="post" class="needs-validation" novalidate>
          @csrf 
          <div class="row">
            <div class="col-lg-12">

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('គោត្តនាម និងនាម *') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="name" class="form-control form-control-sm" placeholder="{{ __('Your name') }}" id="validationCustom01" require>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('លេខសម្គាល់') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="id" class="form-control form-control-sm ID" placeholder="{{ __('ID Code') }}" id="validationCustom01 check" require>
                  <span class="error-msg text-danger" style="display: none;" id="err">ID is ready taken</span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('មុខដំណែង') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="role" id="validationCustom01" require>
                    <option value="">Select Role</option>
                    @foreach($role as $keyrole)
                      <option value="{{$keyrole->RolesID}}">{{$keyrole->RolesName}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ភេទ') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="sex" id="validationCustom01" require>
                    <option value="">Gender</option>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('សញ្ញាបត្រ') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="major" id="validationCustom01" require>
                    <option value="">Select Major</option>
                    @foreach($major as $key)
                      <option value="{{$key->MajorID}}">{{$key->MajorName}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ថ្នាក់') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="class" id="" require>
                  <option value="">Select Class</option>
                    @foreach($class as $key)
                      <option value="{{$key->calssID}}">{{$key->ClassName}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ជុំនាន់') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="gn" id="gn" require>
                    <option value="">Select Generation</option>
                    @foreach($gen as $key)
                      <option value="{{$key->GenerationID}}">{{$key->Generationname}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ក្រុម') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="gp" id="gp" require>
                    <option value="">-</option>

                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>
              <!-- <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ពេល  ') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="id" class="form-control form-control-sm" placeholder="{{ __('time  ') }}">
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div> -->

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('លេខសម្ងាត់  ') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="pass" class="form-control form-control-sm" placeholder="{{ __('Password ') }}" id="validationCustom01" require>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>


            </div>
            
            &emsp;
            <div class="col-12">
              <div class="form-group d-flex justify-content-end">
                <button type="submit" class="btn btn-info" name="submit" Value="Save&New">
                  <i class="icon-file-plus2 mr-1"></i>{{ __('save') }}
                </button>

              </div>
            </div>
          </div>
        </div>
        </form>


      </div>
      <script>
          document.getElementsByClassName("ID")[0].addEventListener('keyup',function(e)
          {
            $.ajaxSetup
            ({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
            });
            e.preventDefault();
                var type = "get";
                var ajaxurl = 'checkid';
                var fdata ={ id:$(".ID").val()}
                // alert(fdata)
                $.ajax({
                 
                 type: type,
                 url: ajaxurl,
                 data: fdata,

                 success: function(respone) {
                    if(respone==fdata.id){
                      document.getElementById('err').style.display=''
                      document.getElementsByClassName('ID')[0].style.border='1px solid red'
                    }
                   if(respone!=fdata.id){
                      document.getElementById('err').style.display='none'
                      document.getElementsByClassName('ID')[0].style.border=''
                    }
                 },

             });
                
          })
        $(document).ready(function() {
            $("#gn").change(function(e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                e.preventDefault();
                var type = "get";
                var ajaxurl = 'toup';
                var fdata = {
                    test: $("#gn").val()
                }
                $.ajax({
                 
                    type: type,
                    url: ajaxurl,
                    data: fdata,

                    success: function(respone) {
                        
                        $("#gp").html(respone)
            
                    },

                });
            });
        })
      </script>
@stop