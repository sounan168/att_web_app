@extends('layout')


@section('leftnav')
All student
@stop
@section('content')
<div class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#">Search</a>
                        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button> -->

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                        Dropdown
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled">Disabled</a>
                                </li>
                            </ul> -->
                            <form class="form-inline my-2 my-lg-0" method="get" action="{{url('/allstd')}}">
                            @csrf 
                                <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
                                <select name="major" id="" class="form-control mr-sm-2">
                                    <option value="">-Major-</option>
                                    @foreach($major as $key)
                                        <option value="{{$key->MajorID}}">{{$key->MajorName}}</option>
                                    @endforeach
                                </select>
                                <button class="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </nav>
<table class="table">
  <thead>
  </thead>
  <tbody class="table-group-divider table-divider-color">
   @foreach($data as $key)
    <tr>
      <td class="text-left">
        <div class="d-flex align-items-center">
        @if($key->{'Profile'}==null)
                                        <img src="/dist/img/no_avatar.png" style="width: 60px; height: 60px" class="rounded-circle img-fluid img-thumbnail " />
                                        @elseif($key->{'Profile'}!=null)
                                        <img src="{{asset('image/'.$key->Profile.'')}}" style="width: 60px; height: 60px" class="rounded-circle img-fluid img-thumbnail " />
                                        @endif
       <p class="h5 ml-3  mb-0">
          
         <strong>{{$key->UserName}}</strong>
         <br>
         <span style="font-size: 20px;">ID:{{$key->UserID}}</span>
         <br>
         <small class="text-muted h5">{{$key->MajorName}}</small>
          
       </p>
       
        </div>
      
      
      </td>
      <td  class="text-right"><a href="{{url('/view_atten/'.$key->UserID.'')}}" class="btn btn-info" >View</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
{{$data->links()}}
</div>
@stop