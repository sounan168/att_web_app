<!DOCTYPE html>
<style>
  /* .center {
        display: block;
  margin-left: auto;
  margin-right: auto;
width: 350px;
} */
</style>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Update</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="/plugins/summernote/summernote-bs4.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/linways/table-to-excel@v1.0.4/dist/tableToExcel.js"></script>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
 
<form action="{{url('/update_pro')}}" method="post" class="needs-validation" novalidate>
          @csrf 
          <div class="row">
            <div class="col-lg-12">

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('គោត្តនាម និងនាម *') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="UserName" value="{{$data[0]->UserName}}" class="form-control form-control-sm" placeholder="{{ __('Your name') }}" id="validationCustom01" require>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('លេខសម្គាល់') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="UserID" value="{{$data[0]->UserID}}" class="form-control form-control-sm" placeholder="{{ __('ID Code') }}" id="validationCustom01" require>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('មុខដំណែង') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="RolesID" id="validationCustom01" require>
                    <option value="">Select Role</option>
                    @foreach($role as $keyrole)
                      <option value="{{$keyrole->RolesID}}" {{$data[0]->RolesID== $keyrole->RolesID  ? 'selected':''; }}>{{$keyrole->RolesName}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ភេទ') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="Sex" id="validationCustom01" require>
                    <option value="">Gender</option>
                    <option value="M" {{$data[0]->Sex == 'M' ? 'selected' : '';}} >Male</option>
                    <option value="F" {{$data[0]->Sex == 'F' ? 'selected' : '';}} >Female</option>
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('សញ្ញាបត្រ') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="MajorID" id="validationCustom01" require>
                    <option value="">Select Major</option>
                    @foreach($major as $key)
                      <option value="{{$key->MajorID}}" {{$data[0]->MajorID == $key->MajorID ? 'selected' : '';}}>{{$key->MajorName}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ជុំនាន់') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="gn" id="gn" require>
                    <option value="">Select Generation</option>
                    @foreach($gen as $key)
                      <option value="{{$key->GenerationID}}"{{$data[0]->GenerationID == $key->GenerationID ? 'selected' : '';}}>{{$key->Generationname}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ក្រុម') }}</label>
                <div class="col-lg-9">
                  <select class="form-control" name="gp" id="gp" require>
                    <option value="">-</option>
                    @foreach($gp as $key)
                      <option value="{{$key->GroupID}}"{{$data[0]->GroupID == $key->GroupID ? 'selected' : '';}}>{{$key->GroupID}}</option>
                    @endforeach
                  </select>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>

              <!-- <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('ពេល  ') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="id" class="form-control form-control-sm" placeholder="{{ __('time  ') }}">
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div> -->

              <div class="form-group row">
                <label class="form-label col-form-label col-lg-3 required">{{ __('លេខសម្ងាត់  ') }}</label>
                <div class="col-lg-9">
                  <input type="text" name="Passwords" value="{{$data[0]->Passwords}}" class="form-control form-control-sm" placeholder="{{ __('Password ') }}" id="validationCustom01" require>
                  <span class="error-msg hidden" id="room_error"></span>
                </div>
              </div>


            </div>
            
            &emsp;
            <div class="col-12">
              <div class="form-group d-flex justify-content-end">
                <button type="submit" class="btn btn-info" name="submit" Value="Save&New">
                  <i class="icon-file-plus2 mr-1"></i>{{ __('save') }}
                </button>

              </div>
            </div>
          </div>
        </div>
        </form>

  <script src="/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script>
        var f= $("#gn").val()
        $(document).ready(function() {
        
            $("#gn").on("change",function(e) {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                    }
                });
                // e.preventDefault();
                var type = "get";
                var ajaxurl = 'toup';
                var fdata = {
                    test: $("#gn").val()
                }
                $.ajax({
                 
                    type: type,
                    url: ajaxurl,
                    data: fdata,

                    success: function(respone) {
                        
                        $("#gp").html(respone)
                        // alert('change')
            
                    },
                    error:function(e){
                      console.log(e)
                    }

                });
            });
            $.widget.bridge('uibutton', $.ui.button)
        })
      
    
  </script>
  <!-- Bootstrap 4 -->
  <script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="/plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="/plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="/plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="/plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="/plugins/moment/moment.min.js"></script>
  <script src="/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="/plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="/dist/js/adminlte.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="/dist/js/demo.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="/dist/js/pages/dashboard.js"></script>

</body>
</html>