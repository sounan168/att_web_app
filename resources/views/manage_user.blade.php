@extends('layout')
@section('leftnav')
Manage User
@stop
@section('content')
                <div class="container">
                    <!-- nav serch user -->

                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <a class="navbar-brand" href="#">Search</a>
                        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button> -->

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- <ul class="navbar-nav mr-auto">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Link</a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                                        Dropdown
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link disabled">Disabled</a>
                                </li>
                            </ul> -->
                            <form class="form-inline my-2 my-lg-0" method="get" action="{{url('/manage_user')}}">
                            @csrf 
                                <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
                                <select name="roles" id="" class="form-control mr-sm-2">
                                    <option value="">-Role-</option>
                                    @foreach($role as $key)
                                        <option value="{{$key->RolesID}}">{{$key->RolesName}}</option>
                                    @endforeach
                                </select>
                                <select name="major" id="" class="form-control mr-sm-2">
                                    <option value="">-Major-</option>
                                    @foreach($major as $key)
                                        <option value="{{$key->MajorID}}">{{$key->MajorName}}</option>
                                    @endforeach
                                </select>
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                        </div>
                    </nav>
                    
           
                    <table class="table align-middle mb-0 bg-white">
                        <thead class="bg-light">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Generation</th>
                                <th>Role</th>
                                <th>Major</th>
                                <th>Passwords</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                       
                          
                            
                            @foreach($data as $key)
                            <tr>
                                <td>
                                    <div class="d-flex align-items-center">
                                        <p class="fw-normal mb-1">{{$key->UserID}}</p>

                                </td>
                                <td>
                                    <div class="d-flex ">
                                        <!--name-->
                                        @if($key->{'Profile'}==null)
                                        <img src="/dist/img/no_avatar.png" style="width: 45px; height: 45px" class="rounded-circle img-fluid img-thumbnail " />
                                        @elseif($key->{'Profile'}!=null)
                                        <img src="{{asset('image/'.$key->Profile.'')}}" style="width: 45px; height: 45px" class="rounded-circle img-fluid img-thumbnail " />
                                        @endif
                                        <div class="ml-3">
                                            <p class="fw-bold mb-1">{{$key->UserName}}</p>
                                        </div>
                                    </div>
                                </td>

                                <td>
                                    <p class="fw-bold mb-1">{{$key->GroupID}}</p>
                                </td>
                                <td>
                                    <p class="fw-bold mb-1">{{$key->Generationname}}</p>
                                </td>
                                <td>
                                    <p class="fw-bold mb-1">{{$key->RolesName}}</p>
                                </td>
                                <td>
                                    <p class="fw-bold mb-1">{{$key->MajorName}}</p>
                                </td>
                                <td>
                                    <p class="fw-bold mb-1">{{$key->Passwords}}</p>
                                </td>
                                <td>
                                    <a  href="{{url('/update?id='.$key->UserID)}}" class=" btn btn-warning btn-sm btn-rounded">
                                        Update
                                    </a>
                                    <a href="{{url('/delete/'.$key->UserID)}}" class="btn btn-danger btn-sm btn-rounded">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                         @endforeach
                        </tbody>
                    </table>
                    {{$data->links()}}
                </div>
@stop
           