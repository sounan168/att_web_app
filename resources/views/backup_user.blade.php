@extends('layout')
@section('leftnav')
Backup User
@stop
@section('content')
                <div class="container">


                    <table class="table align-middle mb-0 bg-white">
                        <thead class="bg-light">
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Group</th>
                                <th>Generation</th>
                                <th>Role</th>
                                <th>Major</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key)
                            <tr>
                            <td>
                                <div class="d-flex align-items-center">
                                    <p class="fw-normal mb-1">{{$key->UserID}}</p> 
                                  
                            </td>
                                <td>
                                    <div class="d-flex ">
                                        <!--name-->
                                        @if($key->{'Profile'}==null)
                                        <img src="/dist/img/no_avatar.png" style="width: 45px; height: 45px" class="rounded-circle img-fluid img-thumbnail " />
                                        @elseif($key->{'Profile'}!=null)
                                        <img src="{{asset('image/'.$key->Profile.'')}}" style="width: 45px; height: 45px" class="rounded-circle img-fluid img-thumbnail " />
                                        @endif
                                        <div class="ml-3">
                                            <p class="fw-bold mb-1">{{$key->UserName}}</p>
                                        </div>
                                    </div>
                                </td>
                                
                                <td><p class="fw-bold mb-1">{{$key->GroupID}}</p></td>
                                <td><p class="fw-bold mb-1">{{$key->Generationname}}</p></td>
                                <td><p class="fw-bold mb-1">{{$key->RolesName}}</p></td>
                                <td><p class="fw-bold mb-1">{{$key->MajorName}}</p></td>
                                <td>
                                    
                                    <a href="{{url('/backup_pro/'.$key->UserID.'')}}" class="btn btn-warning btn-sm btn-rounded">
                                        Backup
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
@stop