@extends('layout')

@section('leftnav')
Attendance
@stop
@section('content')
<style>
   
</style>
<div class="mt-5 row" style="min-height: 100%;">

    <div class="col-md-12 d-flex justify-content-center" style="column-gap: 50px;">

        <div class="card" style="width: 18rem;">
            <div class="card-body bg-warning rounded">
                <div class="d-flex justify-content-center pt-4 pb-4" style="color: white;">
                <span style="font-size: 23px;" ><i class="fa-solid fa-users"></i></span>
                <h3 class="ml-2">All Studen</h3>
                </div>
                <div style="color: white;">
                <a href="{{url('/allstd')}}"  class="btn btn-primary" style="color: white;" >View</a>
                </div>
                
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <div class="card-body bg-info rounded">
                <div class="d-flex justify-content-center pt-4 pb-4">
                <span style="font-size: 23px;" ><i class="fa-solid fa-chalkboard-user"></i></i></span>
                <h3 class="ml-2">All Teacher</h3>
                </div>
                
                <a href="{{url('/allteacher')}}" class="btn btn-primary">View</a>
            </div>
        </div>

        <div class="card" style="width: 18rem;">
            <div class="card-body bg-danger rounded">
                <div class="d-flex justify-content-center pt-4 pb-4">
                <span style="font-size: 23px;" ><i class="fa-solid fa-graduation-cap"></i></i></span>
                <h3 class="ml-2">Class</h3>
                </div>
                
                <a href="#" class="btn btn-primary">View</a>
            </div>
        </div>

    </div>

</div>
@stop